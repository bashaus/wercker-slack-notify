### 0.0.3

The pipeline URL in `$WERCKER_RUN_URL` is actually a hash-URL with a different
pattern, so adding a new pattern.

&nbsp;

### 0.0.2

Because the pipeline name for the first step cannot be identified by
`$WERCKER_DEPLOYTARGET_NAME`, we now parse `$WERCKER_RUN_URL` as the pipeline
name is in the URL.

If the pipeline name cannot be identified from `$WERCKER_RUN_URL`, it falls
back to `build`.

&nbsp;

### 0.0.1

Initial release.

Provides the following configuration options:

* `webhook-url`: The Slack webhook URL to interface with your slack team.
* `message-channel`: The name of the channel in which messages will appear.
* `message-username`: The username of the bot, messages will be received from this user.
* `message-icon-url`: The URL of the icon that should be provided alongside the chat message.
* `message-on-passed`: Boolean as to whether or not a message should be sent if the pipeline passes.
* `message-on-failed`: Boolean as to whether or not a message should be sent if the pipeline fails.
* `message-pretext`: The pretext shown before the message.
* `message-text-on-passed`: The text that should be used if a pipeline passes.
* `message-text-on-failed`: The text that should be used if a pipeline fails.

&nbsp;
